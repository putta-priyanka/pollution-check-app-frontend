import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginGuard } from './gaurds/login.guard';
import { SearchByCityComponent } from './Modules/applicationModule/components/search-by-city/search-by-city.component';
import { DashboardComponent } from './Modules/frontendModule/dashboard/dashboard.component';
import { WatchComponent } from './Modules/frontendModule/watch/watch.component';



import { LoginComponent } from './Modules/userModule/login/login.component';
import { RegisterComponent } from './Modules/userModule/register/register.component';
import { TipsComponent } from './tips/tips.component';
import { LogoutComponent } from './logout/logout.component';
import { HeaderComponent } from './header/header.component';
import { AboutComponent } from './about/about.component';
import { ListCitiesPollutionComponent } from './Modules/watchlistModule/list-cities-pollution/list-cities-pollution.component';



const routes: Routes = [
  {
     path: '', component: LoginComponent 
    },
  
  {
    path: 'dashboard',component: DashboardComponent , canActivate:[LoginGuard]
  },
  {
    path: 'register',component: RegisterComponent
  },
  {
    path: 'watch', component:WatchComponent , canActivate:[LoginGuard]
  },
  {
    path: 'about', component:AboutComponent
  },
  {
    path: 'tips', component:TipsComponent
  },
  {
    path: 'logout', component:LogoutComponent
  },
  {
    path: 'header', component:HeaderComponent
  }
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
